using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_Camara : MonoBehaviour
{
    public GameObject Jugador;
    public float xOffset, yOffset, zOffset;

    // Update is called once per frame
    void Update()
    {
        transform.position = Jugador.transform.position + new Vector3(xOffset, yOffset, zOffset);
        transform.LookAt(Jugador.transform.position);
    }
}
