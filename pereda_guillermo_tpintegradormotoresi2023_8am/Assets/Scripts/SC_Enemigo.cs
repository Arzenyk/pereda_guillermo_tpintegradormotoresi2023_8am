using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_Enemigo : MonoBehaviour
{
    [SerializeField] GameObject[] Waypoint;
    int currentWaypointIndex = 0;
    [SerializeField] float velocidad = 1f;

    void Update()
    {
        if (Vector3.Distance(transform.position, Waypoint[currentWaypointIndex].transform.position) < .1f)
        {
            currentWaypointIndex++;

            if (currentWaypointIndex >= Waypoint.Length)
            {
                currentWaypointIndex = 0;
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, Waypoint[currentWaypointIndex].transform.position, velocidad * Time.deltaTime);
    }
}
