using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class SC_Jugador : MonoBehaviour
{
    public Rigidbody rb;
    public float movSpeed = 10f;
    public GameObject objMeta;
    public GameObject objMoneda;
    public GameObject objEnemigo;
    public GameObject objTrampa;
    public GameObject txtGanaste;
    public GameObject txtRestart;
    //public GameObject ParticulasMoneda;
    public TextMeshProUGUI txtContador;

    private int count;
    private float xInput;
    private float zInput;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        SC_GestorDeAudio.instancia.ReproducirSonido("Fondo");

        objMeta.SetActive(false);
        txtGanaste.SetActive(false);
        txtRestart.SetActive(false);
        count = 0;
        SettxtContador();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInputs();
    }

    private void FixedUpdate()
    {
        Move();

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void ProcessInputs()
    {
        xInput = Input.GetAxis("Horizontal");
        zInput = Input.GetAxis("Vertical");
    }

    private void Move()
    {
        rb.AddForce(new Vector3(xInput, 0f, zInput) * movSpeed);
    }

    void SettxtContador()
    {
        txtContador.text = "Monedas: " + count.ToString();

        if (count >= 5)
        {
            objMeta.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Moneda"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SettxtContador();
            SC_GestorDeAudio.instancia.ReproducirSonido("Moneda");
        }

        if (other.gameObject.CompareTag("Enemigo"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            SC_GestorDeAudio.instancia.ReproducirSonido("Fail");
        }

        if (other.gameObject.CompareTag("Trampa"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            SC_GestorDeAudio.instancia.ReproducirSonido("Fail");
        }

        if (other.gameObject.CompareTag("Meta"))
        {
            txtGanaste.SetActive(true);
            txtRestart.SetActive(true);
            SC_GestorDeAudio.instancia.ReproducirSonido("Win");
        }

        /*
        if (other.gameObject.CompareTag("ParticulasMoneda"))
        {
            ParticulasMoneda.SetActive(false);
        }
        */
    }
}
